import React, {
  createContext,
  useState,
  useContext,
  ReactNode,
  useEffect,
  useMemo,
} from 'react';
import { Theme, ThemeColors, themes } from './theme';
type Context = {
  theme: ThemeColors;
  toggleTheme: (theme: Theme) => void;
};

const ThemeContext = createContext<Context | undefined>(undefined);

export const useTheme = () => {
  const context = useContext(ThemeContext);
  if (!context) {
    throw new Error('useTheme must be used within a ThemeProvider');
  }
  return context;
};

export const ThemeProvider = ({ children }: { children: ReactNode }) => {
  const [activeTheme, setActiveTheme] = useState<Theme>(Theme.BLUE);

  const toggleTheme = (theme: Theme) => {
    setActiveTheme(theme);
  };
  const contextValue: Context = useMemo(
    () => ({
      theme: themes[activeTheme],
      toggleTheme,
    }),
    [activeTheme, toggleTheme]
  );
  useEffect(() => {
    console.log('activeTheme');
  }, [activeTheme]);
  return (
    <ThemeContext.Provider value={contextValue}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeContext;

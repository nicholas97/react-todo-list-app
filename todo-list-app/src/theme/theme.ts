export type ThemeColors = {
  mainColor: string;
  secondaryColor: string;
};
export enum Theme {
  BLUE,
  PURPLE,
  GREEN,
  ORANGE,
}
type ThemesColors = {
  [key in Theme]: ThemeColors;
};
export const themes: ThemesColors = {
  [Theme.BLUE]: {
    mainColor: '#0095ff',
    secondaryColor: ' #d7eeff',
  },
  [Theme.PURPLE]: {
    mainColor: '#9000ff',
    secondaryColor: '#eed9ff',
  },
  [Theme.GREEN]: {
    mainColor: '#64bc00',
    secondaryColor: '#e5ffc8',
  },
  [Theme.ORANGE]: {
    mainColor: '#ff8400',
    secondaryColor: '#ffdfbc',
  },
};

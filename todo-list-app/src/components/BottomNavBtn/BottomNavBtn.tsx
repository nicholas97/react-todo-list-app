import React, { memo } from 'react';
import './BottomNavBtn.scss';
import { Theme, themes } from '../../theme/theme';
import { useTheme } from '../../theme/themeContext';
export type BottomNavBtnParams = {
  id: number;
  text: string;
  themeKey: Theme;
  isSelected: boolean;
  onClick: (id: number) => void;
};

function BottomNavBtn({
  id,
  text,
  themeKey,
  isSelected,
  onClick,
}: BottomNavBtnParams) {
  const theme = themes[themeKey];
  return (
    <div className='bottom-navbar-btn-wrapper'>
      <div
        className={'btn'}
        style={{ backgroundColor: `${isSelected ? theme.secondaryColor : ''}` }}
        onClick={() => {
          onClick(id);
        }}
      >
        <div
          className='btn-icon'
          style={{
            backgroundColor: theme.mainColor,
          }}
        ></div>
        <div
          className='btn-text'
          style={{
            color: theme.mainColor,
          }}
        >
          {text}
        </div>
      </div>
    </div>
  );
}

export default memo(BottomNavBtn);

import React, { memo, useState } from 'react';
import './BottomNav.scss';
import BottomNavBtn from '../BottomNavBtn/BottomNavBtn';
import { Theme } from '../../theme/theme';
import { useTheme } from '../../theme/themeContext';
export type BottomNavBtns = { text: string; themeKey: Theme };
export type BottomNavParams = {
  defaultSelectedBtn?: number;
  btns: BottomNavBtns[];
  onSelect: (btn: BottomNavBtns) => void;
};

function BottomNav({
  defaultSelectedBtn = 0,
  btns,
  onSelect,
}: BottomNavParams) {
  const [selectedBtn, setSelectedBtn] = useState<number>(defaultSelectedBtn);
  const { toggleTheme } = useTheme();
  return (
    <div className='bottom-navbar-wrapper'>
      {btns.map((element, index) => (
        <BottomNavBtn
          key={index}
          id={index}
          themeKey={element.themeKey}
          text={element.text}
          isSelected={index === selectedBtn}
          onClick={(id: number) => {
            toggleTheme(btns[id].themeKey);
            setSelectedBtn(id);
            onSelect({
              ...btns[id],
            });
          }}
        />
      ))}
    </div>
  );
}

export default memo(BottomNav);

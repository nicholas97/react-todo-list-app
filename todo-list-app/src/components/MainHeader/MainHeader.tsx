import React, { memo } from 'react';
import './MainHeader.scss';
import { Theme, themes } from '../../theme/theme';
import { useTheme } from '../../theme/themeContext';
export type MainHeaderParams = {
  title: string;
};

function MainHeader({ title }: MainHeaderParams) {
  const { theme } = useTheme();
  const sideBarOnClick = () => {
    console.log('sideBarOnClick');
  };
  return (
    <div
      className='main-header-wrapper'
      style={{ background: theme.mainColor }}
    >
      <div
        className='side-bar-btn'
        onClick={sideBarOnClick}
      >
        <div className='line'></div>
        <div className='line'></div>
        <div className='line'></div>
      </div>
      <div className='title'>{title}</div>
    </div>
  );
}

export default memo(MainHeader);

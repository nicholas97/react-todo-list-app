import React, { memo } from 'react';
import logo from './logo.svg';
import './App.css';
import BottomNav, { BottomNavBtns } from './components/BottomNav/BottomNav';
import { Theme } from './theme/theme';
import { ThemeProvider, useTheme } from './theme/themeContext';
import MainHeader from './components/MainHeader/MainHeader';

function App() {
  return (
    <ThemeProvider>
      <MainHeader title='TODO' />
      <BottomNav
        btns={[
          { text: '蓝色', themeKey: Theme.BLUE },
          { text: '紫色', themeKey: Theme.PURPLE },
          { text: '绿色', themeKey: Theme.GREEN },
          { text: '橙色', themeKey: Theme.ORANGE },
        ]}
        onSelect={function (btn: BottomNavBtns): void {
          console.log(btn);
        }}
      />
    </ThemeProvider>
  );
}

export default memo(App);
